package com.training.droidbountyhunter.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.training.droidbountyhunter.fragments.PlaceholderFragment

class SectionsPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment =
        PlaceholderFragment.newInstance(position + 1)

    // Show 3 total pages.
    override fun getCount(): Int = 3
}
